package com.adminpanel.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ManyToAny;

import lombok.Data;

@Data
@Entity
public class Department {

	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id ;
	
	@OneToOne
	User HOD;
	
	private String deptName ;
	
	private String type ;
	
	private String mis ;
}
