package com.adminpanel.serviceImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.adminpanel.dto.CreateDepartmentRequestDto;
import com.adminpanel.dto.CreateUserRequestDto;
import com.adminpanel.dto.UpadateDepartmentRequestDto;
import com.adminpanel.entities.Department;
import com.adminpanel.entities.User;
import com.adminpanel.enums.DepartmentType;
import com.adminpanel.enums.UserType;
import com.adminpanel.repository.DepartmentRepository;
import com.adminpanel.repository.UserRepository;
import com.adminpanel.service.AdminService;

@Service
public class AdminServiceImpl implements AdminService{

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	DepartmentRepository departmentRepository;
	
	@Override
	public Map<String, String> saveUser(CreateUserRequestDto dto) {
		// TODO Auto-generated method stub\
		
		Map<String , String > response =new HashMap<>();
		
		User user =new User();
		
		user.setName(dto.getName());
		
		user.setUserType(dto.getType());
		
		userRepository.save(user);
		
		response.put("Message ", "Saved!!!");
		
		return response;
	}

	@Override
	public Map<String, String> updateUser(User dto) {
		// TODO Auto-generated method stub
		Map<String , String > response =new HashMap<>();
	User user=	userRepository.findById(dto.getId()).orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND,"user not found !!!"));
		
		if(dto.getName()!=null)
			user.setName(dto.getName());
		if(dto.getUserType()!=null)
			user.setUserType(dto.getUserType());
		
		userRepository.save(user);
		
		response.put("Message", "Updated!!!");
		
		return response ;
	}

	@Override
	public Map<String, String> saveDepartment(CreateDepartmentRequestDto dto) {
		// TODO Auto-generated method stub
		boolean statusFlag=false;
		System.out.println("----------------------------------------------"+DepartmentType.BILLABLE.getType());
		Map<String, String> response =new HashMap<>();
		
		if(DepartmentType.BILLABLE.getType()==dto.getType() || DepartmentType.NON_BILLABLE.getType()==dto.getType())
		statusFlag=true;	
		
		if(statusFlag)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Department Type Not Valid !!!");
		
		User hod=	userRepository.findById(dto.getHodId()).orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND,"user not found !!!"));
		
		Department department = new Department();
		
		department.setDeptName(dto.getDeptName());
		department.setHOD(hod);
		department.setMis(dto.getMis());
		department.setType(dto.getType());
		
		
		departmentRepository.save(department);
		
		response.put("Message", "Departmenat Saved !!!");
		return response ;
	}

	@Override
	public Map<String, String> upadateDepartment(UpadateDepartmentRequestDto dto) {
		// TODO Auto-generated method stub
		Map<String , String > response =new HashMap<>();
		Department department=departmentRepository.findById(dto.getId())
				.orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND,"Department Not Found!!!"));
	
		
		if(dto.getDeptName()!=null)
			department.setDeptName(dto.getDeptName());
		if(dto.getHOD()!=null) {
			User hod=userRepository.findById(dto.getHOD())
					.orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND,"Hod not Found!!!"));
			department.setHOD(hod);
			}
		if(dto.getMis()!=null)
			department.setMis(dto.getMis());
		if(dto.getType()!=null)
			department.setType(dto.getType());
		
		departmentRepository.save(department);
		
		
		response.put("Message", "Updated!!!");
		return response;
	}

	
	
	@Override
	public Map<String, String> deleteDepartment(Long id) {
		// TODO Auto-generated method stub
		Map<String , String > response =new HashMap<>();
		
	Department department=departmentRepository.findById(id)
		.orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND,"Department Not Found !!!"));
		
		departmentRepository.delete(department);
		
		response.put("Message", "Deleted!!!");
		
		return response;
	}

	@Override
	public Department getDepartmentByid(Long id) {
		// TODO Auto-generated method stub
		
		Department department=departmentRepository.findById(id )
				.orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND,"Department Not Found !!!"));
		
		return department;
	}

	@Override
	public List<Department> getDeartmentByType(String type) {
		// TODO Auto-generated method stub
		
	return	departmentRepository.findByType(type);
	}

	@Override
	public List<Department> getAllDepartment() {
		// TODO Auto-generated method stub
		
		return departmentRepository.findAll();
	}

	@Override
	public List<User> getAllUser() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}
	
	
}
