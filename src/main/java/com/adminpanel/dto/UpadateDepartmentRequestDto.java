package com.adminpanel.dto;

import javax.persistence.OneToOne;

import com.adminpanel.entities.User;

import lombok.Data;

@Data
public class UpadateDepartmentRequestDto {

	
	private Long id ;
	
	private Long  HOD;
	
	private String deptName ;
	
	private String type ;
	
	private String mis ;
}
