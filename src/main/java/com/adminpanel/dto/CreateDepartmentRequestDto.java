package com.adminpanel.dto;

import lombok.Data;

@Data
public class CreateDepartmentRequestDto {

	private Long hodId;
	
	private String deptName ;
	
	private String type ;
	
	private String mis ;
	
}
