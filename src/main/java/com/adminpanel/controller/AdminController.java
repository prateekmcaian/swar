package com.adminpanel.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.adminpanel.dto.CreateDepartmentRequestDto;
import com.adminpanel.dto.CreateUserRequestDto;
import com.adminpanel.dto.UpadateDepartmentRequestDto;
import com.adminpanel.entities.Department;
import com.adminpanel.entities.User;
import com.adminpanel.repository.DepartmentRepository;
import com.adminpanel.repository.UserRepository;
import com.adminpanel.service.AdminService;

@RestController
public class AdminController {

	@Autowired
	AdminService adminService;
	
	
	@PostMapping("/user")
	public Map<String , String > saveUser(@RequestBody CreateUserRequestDto user ){

	return	adminService.saveUser(user);

	}

	@PostMapping("/dept")
	public Map<String , String > saveDepartment(@RequestBody CreateDepartmentRequestDto department) {

		return adminService.saveDepartment(department);
	}
	
	@PutMapping("/user")
	public Map<String , String > updateUser(@RequestBody User user ){return adminService.updateUser(user);}
	
	@PutMapping("/dept")
	public Map<String, String >  upadateDepartment(@RequestBody UpadateDepartmentRequestDto department ){
		
		return adminService.upadateDepartment(department);
	}

	@DeleteMapping("/delete/dept/{id}")
	public Map<String , String > deleteDepartment(@PathVariable Long id ){
		return adminService.deleteDepartment(id);
	}
	
	@GetMapping("/dept/{id}")
	public Department getDepartmentByid(@PathVariable Long id ) {
		return adminService.getDepartmentByid(id);
	}
	
	@GetMapping("/dept/all")
	public List<Department> getAllDepartment(){
		return adminService.getAllDepartment();
	}
	
	@GetMapping("/dept/type/{type}")
	public List<Department > getDeartmentByType(@PathVariable String type ){
		return adminService.getDeartmentByType(type);
	}
	
	
	@GetMapping("/user/all")
	public List<User> getAllUser(){
	return 	adminService.getAllUser();
	}
}
