package com.adminpanel.enums;

public enum UserType {

	HOD("HOD"), ADMIN("ADMIN");

	String hod;

	UserType(String userType) {
		this.hod = userType;
	}

	public String getUserType() {
		return this.hod;
	}
	
}
