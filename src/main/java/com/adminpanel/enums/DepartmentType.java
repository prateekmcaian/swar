package com.adminpanel.enums;

import lombok.Data;


public enum DepartmentType {

	BILLABLE("BILLABLE"),NON_BILLABLE("NON_BILLABLE");
	
	String type ;
	
	DepartmentType(String type ){
	this.type=type;	
	}
	
	public String getType() {
		return this.type;
	}
}
