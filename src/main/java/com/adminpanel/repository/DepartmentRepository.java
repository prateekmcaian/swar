package com.adminpanel.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adminpanel.entities.Department;

@Repository
public interface DepartmentRepository extends JpaRepository<Department	, Long >{
	
	List<Department> findByType(String type);
}
