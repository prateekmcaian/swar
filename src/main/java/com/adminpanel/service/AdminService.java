package com.adminpanel.service;

import java.util.List;
import java.util.Map;

import com.adminpanel.dto.CreateDepartmentRequestDto;
import com.adminpanel.dto.CreateUserRequestDto;
import com.adminpanel.dto.UpadateDepartmentRequestDto;
import com.adminpanel.entities.Department;
import com.adminpanel.entities.User;

public interface AdminService {

	public Map<String , String > saveUser(CreateUserRequestDto user );
	
	public Map<String , String > updateUser(User user );
	
	public Map<String , String > saveDepartment(CreateDepartmentRequestDto department);
	
	public Map<String, String >  upadateDepartment(UpadateDepartmentRequestDto department );
	
	public Map<String , String > deleteDepartment(Long id );
	
	public Department getDepartmentByid(Long id );
	
	public List<Department > getDeartmentByType(String type );
	
	public List<Department> getAllDepartment();
	
	public List<User> getAllUser();
	
}
